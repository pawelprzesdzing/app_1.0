package pl.sda.DAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sda.Domain.Person;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Java6Assertions.assertThat;

class PersonDAOImplTest {
    private PersonDAOImpl personDAO;

    @BeforeEach
    public void setUp() {
        personDAO = new PersonDAOImpl();
    }

    @Test
    public void shouldReturnAllPersonFromDB() {
        List<Person> all = personDAO.findAll();

        assertThat(all).hasSize(1);
    }

    @Test
    public void shouldReturnPersonById() {
        Optional<Person> byId = personDAO.findById(3);

        assertThat(byId.get()).isNotNull();
        assertThat(byId.get().getName()).isEqualTo("Jan");
        assertThat(byId.get().getAge()).isEqualTo(45);
    }

}