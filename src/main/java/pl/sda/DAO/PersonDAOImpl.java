package pl.sda.DAO;

import pl.sda.Domain.Person;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class PersonDAOImpl implements PersonDao {

    private static final EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("person-db");
    }

    @Override
    public List<Person> findAll() {
        EntityManager entityManager = emf.createEntityManager();
        TypedQuery<Person> from_person = entityManager.createQuery("FROM Person", Person.class);
        List<Person> resultList = from_person.getResultList();
        resultList.stream()
                .forEach(System.out::println);
        entityManager.close();
        return resultList;
    }

    @Override
    public Optional<Person> findById(int id) {
        EntityManager entityManager = emf.createEntityManager();
        Person person = entityManager.find(Person.class, id);
        Optional<Person> optionalPerson = Optional.ofNullable(person);
        entityManager.close();
        System.out.println(optionalPerson.get());
        return optionalPerson;
    }

    @Override
    public int create(Person person) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.persist(person);
        entityManager.getTransaction().commit();
        entityManager.close();
        return person.getPersonId();
    }

    @Override
    public int update(Person person) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.merge(person);
        entityManager.getTransaction().commit();
        entityManager.close();
        return person.getPersonId();
    }

    @Override
    public void delete(int id) {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();
        Person person = entityManager.find(Person.class, id);
        entityManager.remove(person);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void deleteAll() {
        EntityManager entityManager = emf.createEntityManager();
        entityManager.getTransaction().begin();

        TypedQuery<Person> from_person = entityManager.createQuery("FROM Person", Person.class);
        Stream<Person> resultList = from_person.getResultStream();

        resultList.forEach(entityManager::remove);

        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public Person findPersonById(int id) {
        EntityManager entityManager = emf.createEntityManager();
        TypedQuery<Person> from_person = entityManager.createQuery("FROM Person", Person.class);
        Optional<Person> resultStream = from_person.getResultStream()
                .filter(x -> x.getPersonId() == id)
                .findAny();
        entityManager.close();
        return resultStream.get();
    }
}
