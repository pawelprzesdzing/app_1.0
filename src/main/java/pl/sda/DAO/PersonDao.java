package pl.sda.DAO;

import pl.sda.Domain.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDao {
    List<Person> findAll();

    Optional<Person> findById(int id);

    int create(Person person);

    int update(Person person);

    void delete(int id);

    void deleteAll();
}
