package pl.sda.Configuration;

import pl.sda.Domain.Address;
import pl.sda.Domain.Person;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Settings {
    private Person person;
    private Scanner scanner = new Scanner(System.in);


    public void showMenu() {
        System.out.println("Select one of option: " +
                "\n1. Display basic info of all people" +
                "\n2. Check details of given person" +
                "\n3. Add person to DB" +
                "\n4. Edit person from DB" +
                "\n5. Remove person from DB" +
                "\n6. Login Panel" +
                "\n7. Exit");
    }

    public Person getCreatePerson() {
        System.out.println("Create person with name, surname, age, height, weight, city, " +
                "zipCode, street, home number - every value cannot be empty");
        person = new Person(scanner.next(), scanner.next(), scanner.nextInt(),
                scanner.nextInt(), scanner.nextDouble(), new Address(scanner.next(), scanner.next(),
                scanner.next(), scanner.nextInt()));
        return person;
    }

    public static void sleepForThreeSecond() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
