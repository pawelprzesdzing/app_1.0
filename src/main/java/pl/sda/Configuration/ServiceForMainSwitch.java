package pl.sda.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.DAO.PersonDAOImpl;
import pl.sda.Domain.Person;

import java.util.NoSuchElementException;
import java.util.Scanner;

import static pl.sda.Configuration.Settings.sleepForThreeSecond;

public class ServiceForMainSwitch {
    private PersonDAOImpl personDAO = new PersonDAOImpl();
    private UserChoice userChoice = new UserChoice();
    private Scanner scanner = new Scanner(System.in);
    private Settings settings = new Settings();

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceForMainSwitch.class);

    public void basicInfoAboutPeople() {
        personDAO.findAll();
        LOGGER.info("Basic info about all people in DB.");
        sleepForThreeSecond();
    }

    public void basicInfoAboutPersonById() {
        try {
            personDAO.findById(userChoice.getUserChoiceId());
            LOGGER.info("Person from DB by id.");
        } catch (NoSuchElementException e) {
            LOGGER.info("There's no person with this ID in DB!");
        } finally {
            sleepForThreeSecond();
        }
    }

    public void createNewPerson() {
        personDAO.create(settings.getCreatePerson());
        LOGGER.info("Person was added to DB.");
        sleepForThreeSecond();
    }

    public void editPersonById() {
        try {
            Person personById = personDAO.findPersonById(userChoice.getUserChoiceId());
            menuToEditPersonsDetails();
            switch (userChoice.getUserChoiceFromMenu()) {
                case 1:
                    System.out.print("Name: ");
                    personById.setName(scanner.next());
                    break;
                case 2:
                    System.out.print("Surname: ");
                    personById.setSurname(scanner.next());
                    break;
                case 3:
                    System.out.print("Age: ");
                    personById.setAge(scanner.nextInt());
                    break;
                case 4:
                    System.out.print("Height: ");
                    personById.setHeight(scanner.nextInt());
                    break;
                case 5:
                    System.out.print("Weight: ");
                    personById.setWeight(scanner.nextDouble());
                    break;
                case 6:
                    System.out.print("City: ");
                    personById.getAddress().setCity(scanner.next());
                    break;
                case 7:
                    System.out.print("ZipCode: ");
                    personById.getAddress().setZipCode(scanner.next());
                    break;
                case 8:
                    System.out.print("Street: ");
                    personById.getAddress().setStreet(scanner.next());
                    break;
                case 9:
                    System.out.print("HomeNumber: ");
                    personById.getAddress().setHomeNumber(scanner.nextInt());
                    break;
                default:
                    LOGGER.info("There's no option like that!");
            }
            personDAO.update(personById);
            LOGGER.info("Person was edited from DB.");
        } catch (NoSuchElementException e) {
            LOGGER.info("There's no person with this ID in DB!");
        } finally {
            sleepForThreeSecond();
        }
    }

    public void deletePersonById() {
        try {
            personDAO.delete(userChoice.getUserChoiceId());
            LOGGER.info("Person was removed from DB.");
            sleepForThreeSecond();
        } catch (IllegalArgumentException e) {
            LOGGER.info("There's no person with this ID in DB!");
            sleepForThreeSecond();
        }
    }

    public void menuToEditPersonsDetails() {
        System.out.println("Choose what do You want do change: " +
                "\n 1. Name" +
                "\n 2. Surname" +
                "\n 3. Age" +
                "\n 4. Height" +
                "\n 5. Weight" +
                "\n 6. City" +
                "\n 7. ZipCode" +
                "\n 8. Street" +
                "\n 9. HomeNumber");
    }
}
