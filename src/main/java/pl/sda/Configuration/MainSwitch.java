package pl.sda.Configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.LoginPanel.LoginPanel;

import java.io.IOException;

import static pl.sda.Configuration.Settings.sleepForThreeSecond;

public class MainSwitch {
    private Settings settings = new Settings();
    private UserChoice userChoice = new UserChoice();
    private ServiceForMainSwitch serviceForMainSwitch = new ServiceForMainSwitch();
    private LoginPanel loginPanel = new LoginPanel();
    private boolean logIn = true;

    private static final Logger LOGGER = LoggerFactory.getLogger(MainSwitch.class);

    public void appUp() throws IOException {
        while (logIn == true) {
            settings.showMenu();
            switch (userChoice.getUserChoiceFromMenu()) {
                case 1:
                    serviceForMainSwitch.basicInfoAboutPeople();
                    break;
                case 2:
                    serviceForMainSwitch.basicInfoAboutPersonById();
                    break;
                case 3:
                    serviceForMainSwitch.createNewPerson();
                    break;
                case 4:
                    serviceForMainSwitch.editPersonById();
                    break;
                case 5:
                    serviceForMainSwitch.deletePersonById();
                    break;
                case 6:
                    loginPanel.logIn();
                    break;
                case 7:
                    logIn = false;
                    LOGGER.info("Exit from app.");
                    break;
                default:
                    LOGGER.info("Wrong number menu! Choose another number from menu.");
                    sleepForThreeSecond();
            }
        }
    }

}
