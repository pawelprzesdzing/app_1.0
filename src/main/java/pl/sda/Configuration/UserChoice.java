package pl.sda.Configuration;

import java.util.Scanner;

public class UserChoice {
    private int userChoiceFromMenu;
    private int userChoiceId;
    private Scanner scanner = new Scanner(System.in);

    public int getUserChoiceFromMenu() {
        return userChoiceFromMenu = scanner.nextInt();
    }

    public int getUserChoiceId() {
        System.out.println("Person ID: ");
        return userChoiceId = scanner.nextInt();
    }

}
