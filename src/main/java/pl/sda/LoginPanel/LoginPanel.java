package pl.sda.LoginPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Scanner;

import static pl.sda.Configuration.Settings.sleepForThreeSecond;

public class LoginPanel {

    private Scanner scanner = new Scanner(System.in);
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginPanel.class);

    public void logIn() throws IOException {
        System.out.println("Login: ");
        String login = scanner.next();
        System.out.println("Password: ");
        String password = scanner.next();

        BufferedReader bufferedReader = getBufferedReader();

        String[] loginFromFile = bufferedReader.readLine().split(": ");
        String[] passwordFromFile = bufferedReader.readLine().split(": ");

        if (login.equals(loginFromFile[1]) && password.equals(passwordFromFile[1])){
            LOGGER.info("Correct login and password!");
        } else {
            LOGGER.info("Wrong login or password!");
        }
        sleepForThreeSecond();
    }

    private BufferedReader getBufferedReader() throws FileNotFoundException {
        File file = new File("C:\\Users\\Paweł\\IdeaProjects\\app_10\\src\\main\\resources\\data.txt");
        return new BufferedReader(new FileReader(file));
    }
}
