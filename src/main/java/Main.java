import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.Configuration.MainSwitch;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        MainSwitch mainSwitch = new MainSwitch();

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("person-db");
        LOGGER.info("App up");
        entityManagerFactory.close();

        mainSwitch.appUp();
    }
}
